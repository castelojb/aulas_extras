# Aulas Extras

## 1. Motivação
Antes de tudo, parabéns por chegar até aqui! Esta é uma coletânea de aulas extras sobre técnicas e ferramentas para 
facilitar sua vida como entusiasta da área de Ciência de Dados. Vamos explorar assuntos desde engenharia de dados a 
visualização de dados, lembrando, como o cliente manda, estou aberto a sugestão de temas que você julgue interessante 
ou sente alguma curiosidade.

## 2. Módulos
Cada módulo é independente do outro, dentro deles, estão explicações mais detalhadas sobre como eles vão abordar cada 
assunto, ferramenta exploradas e linguagem utilizada. Além disso, existirá um arquivo de dependências para que vocẽ
consiga o mesmo ambiente utilizado por mim para executar as aplicações. A ideia é que você veja assuntos que julgue 
interessantes sem precisar passar por outros módulos para isso. 

## 3. Recomendações
Aconselho, fortemente, utilizar o gestor de dependências [Anaconda](https://www.anaconda.com/) ou 
[Miniconda](https://docs.conda.io/en/latest/miniconda.html) para gerenciar suas dependências, leia o 
[tutorial](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) de como utilizar 
eles para gerenciar os ambiente e exportar os arquivos de ambientes. Por fim, veja essas aulas
apenas se estiver interessado, elas possuem um caráter opcional e um sonho de dar gosto pela área.


