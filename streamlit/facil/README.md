# Fácil

## 1. Motivação
Este módulo foi projetado para mostrar como ordenar elementos na página Streamlit, pode parecer fácil à primeira vista, 
e na segunda também, mas isso deve ficar bem claro para quem for projetar uma página, por isso, veja com calma o código 
e assista a aula antes de iniciar a exploração.

## 2. Metodologia

Essa pagina foi desenvolvida para reproduzir o notebook, disponível na pasta *relatório*, apresentado como exemplo da 
atividade 1 da disciplina, utilizando apenas o conceito de instanciação de elementos na página, nada muito complexo. 
Vale ressaltar que o código foi, o máximo possível, documentado para explicar cada função e seus usos.

## 3. Considerações

* O código pode parecer grande, mas isso é apenas pela quantidade de textos na página 
* Não veja _cache_ agora
* Tente fazer sua propria página!