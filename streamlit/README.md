# Streamlit

## 1. Motivação

Este módulo visa apresentar uma ferramenta para prototipação de interfaces com abordagem _low code_, permitindo um 
completo iniciante, desconhecedor da fina arte de fazer uma tela, fazer uma interface para apresentar seus modelos de 
aprendizagem de máquina, relatórios ou, até mesmo, simular novas mecânicas de uma aplicação real -- pausa dramática -- 
vamos conhecer o [Streamlit](https://www.streamlit.io/)!

## 2. Metodologia
O conteúdo foi planejado para ser dividido em 3 aulas (fácil, médio e difícil), antes de se assustar, essa escala foi 
feita sobre as funcionalidades do Streamlit, não sobre a dificuldade real de se aprender elas (iria tudo para o fácil), 
pode respirar agora, o arquivo de dependências será disponibilizado neste diretório.


## 3. Instalando a Ferramenta

1. Abra seu ambiente conda no terminal
2. $ pip install streamlit
3. Pronto!

## 4. Executando uma aplicação Stremalit
Para executar seu código, você deve utilizar o template de linha de comando abaixo


$ streamlit run meu_prog.py --server.port minha_porta --server.baseUrlPath "/minha_base_url/" --server.enableCORS false

## 5. Considerações

1. Leia a [documentação](https://docs.streamlit.io/en/stable/) do Streamlit enquanto faz os modulos
2. Não tenha medo de tirar suas dúvidas com os monitores da disciplina
3. Se divirta!